# Movie Night Trivia Quiz

## Stack

### NodeJs Web Scraper

Directory : web-scraper

https://badge.fury.io/js/imdb-top250-node


### Nodejs - API & DB

Directory : api

Created using FeathersJs


### Socket.io Realtime Messaging Relay

Directory : socket-io-relay

Created using Socket.io & Express


### Reactjs - frontend

Libraries : Tachyons.css, Blueprint ( Icons )

To run : 

1 . Edit ./src/config.json with your API endpoint URL


`{

    "api": "http://localhost:3030",

    "socket_url": "http://localhost:8001"

}
`

To find this url, first deploy the FeathersJS API server to your local or remote VPS.

2 . Run `npm run build` to build the app or start the local react development server using `npm start`

3 . Deploy using the tool of your choice, recommended options are Netlify or Surge.sh

3 . 1 Navigate to `./src/build`
3 . 2 . 1 Install `surge.sh` using `npm install --global surge`, then Deploy using `surge` OR
2 . 2 . 2 Install `netlify-cli` using `npm install netlify-cli -g` then deploy using `netlify deploy`


# Instructions 

1 . Start the FeathersJs app ( DB & API )

1 . 1 Navigate to `./main/server/api`, run `npm install`, then `npm start`
1 . 2 Navigate to `./main/server/socket-io-relay`, run `npm install`, then `npm start`
1 . 3 Now your Database & API, and Realtime Socket IO server are running. 



2 . Run the web scraper app to scrape data from IMBD, convert to JSON and insert into DB. After complete, exit as this is a once off process.

2 . 1 Navigate to `./web-scraper`
2 . 2 run `npm install` to install node_modules
2 . 3 Edit the `config.json` file with the correct API endpoint URL ( or use default config ).

`{
    "api": "http://localhost:3030"
}`

2 . 4 Run the app with `npm start`

2 . 5 Get data :
 
2 . 5 . 1 If running locally, navigate to `http://localhost:3001` in your browser.
2 . 5 . 2 If running on remote server, navigate to `http://<YOUR_IP>:3001` in your browser.

This triggers the web scraping function and inserts the data into the running `FeathersJS NEDB Database` via the `FeathersJS API`.

Load the client app, see `ReactJs - Frontend `


