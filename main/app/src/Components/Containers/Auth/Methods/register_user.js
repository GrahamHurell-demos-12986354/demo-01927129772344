import config from '../../../../Utils/config.js'

const register_user = async({ self, user }) => {

    let url = config.api + '/players/'

    console.log('URL ', url)

    let fetch_config = { 

        method: "POST",

        headers: {

            "Content-Type": "application/json"

        },

        body: JSON.stringify( user )

    }

    await fetch( url, fetch_config )

    .then( res => res.json() )

    .then( async res => {

        console.log('Register User response ====> ',res)

        return res

    })

}
export default register_user
