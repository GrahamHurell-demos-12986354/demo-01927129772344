import React, { PureComponent } from 'react'

import PropTypes from 'prop-types'

import { AppContext } from 'utils'

import io from "socket.io-client"

import { on_login } from './Methods'

class Auth extends PureComponent {

  // socket = null

  constructor(props) {

    super(props)

    this.state = {

      hasError: false,

      isAuthenticated: false,

      player_name: null,
      player_id: null,

      opponent_name: null,
      opponent_id: null,

      user: null,

      players: []

    }


    this.on_login = on_login
    
  }



  // onLogin = (name) => {
    
  //   console.log('login', name)

  // }

  // submitLogin = (name) => {
    
  //   this.socket.emit("login", JSON.stringify(name))

  // }

  // componentDidMount = () => {

  //   this.initSocketConnection()

  //   this.setupSocketListeners()

  //   console.log('Auth mounted ===> ', this.context )

  // }

  // initSocketConnection() {

  //   this.socket = io.connect(SOCKET_URI)

  //   window.socket = this.socket

  // }

  // onLogin = async (message) => {

  //   console.log('onLogin', message)

  //   console.log('Auth : this.context ===> ', this.context)

  //   on_login({ self: this, message })

  // }

  // onQuizAnswer = (data) => {

  //   // alert('onquizanswer')
    
  //   this.socket.emit("message", JSON.stringify(data))

  //   // this.onMessageReceived(data)

  // }

  // onMessageReceived = async(e) => {

  //   console.log('Message Received ===> ', e )

  //   console.log('Auth : this.context ===> ', this.context )

  // }

  // setupSocketListeners = () => {

  //   // this.socket.on("message", (e) => {
  //   //         console.log('message', e)    
  //   // })

    
  //   this.socket.on("message", this.onMessageReceived)
  //   this.socket.on("login", this.onLogin )

  // }

  render() {

    let { isAuthenticated } = this.state

    if (this.state.hasError) {

      return <h1>Something went wrong.</h1>

    }

    return (

      <AppContext.Provider

        value={{

          isAuthenticated,

          Auth: this

        }}

      >

        {this.props.children}

      </AppContext.Provider>

    )
  }
}

export default Auth

Auth.propTypes = {
  // bla: PropTypes.string,
};

Auth.defaultProps = {
  // bla: 'test',
};

Auth.contextType = AppContext

// const mapStateToProps = state => ({
//   // blabla: state.blabla,
// });

// const mapDispatchToProps = dispatch => ({
//   // fnBlaBla: () => dispatch(action.name()),
// });

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(Auth);
