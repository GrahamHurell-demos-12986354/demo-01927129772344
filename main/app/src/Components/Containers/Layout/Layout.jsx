import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Layout.styles';
import { AppContext } from 'utils'

import { Loading } from './Elements'

class Layout extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      
  
      a: false,
      b: false,
      c: false,

      theme: 'green',
      player_scores: false,

      quiz_state: 'normal',
      quiz_response: false,
      quiz_correct: false,

      loading: false,

      hasError: false,
  
    }
  
  }
  render () {

    let { loading, a, b, c, theme, player_scores, quiz_state, quiz_response, quiz_correct } = this.state 

    if (this.state.hasError) {
    
      return <h1>Something went wrong in Layout.jsx</h1>;
    
    }

    return (
      <AppContext.Provider
      
        value={{

          a, b, c,

          theme,

          player_scores, 

          quiz_state,
          quiz_response,
          quiz_correct,

          quiz_active: false, 

          Layout: this,

          ...this.context

        }}

      >

        { loading && <Loading /> }

        { this.props.children }

      </AppContext.Provider>
    );
  }
}

Layout.propTypes = {
  // bla: PropTypes.string,
};

Layout.defaultProps = {
  // bla: 'test',
};

export default Layout;
Layout.contextType = AppContext
