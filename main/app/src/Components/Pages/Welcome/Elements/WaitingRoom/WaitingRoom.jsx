import React from 'react';
import PropTypes from 'prop-types';
//import { Test } from './WaitingRoom.styles';

const WaitingRoom = ({ context, active }) => (
  active && <div className="WaitingRoomWrapper flex flex-column pv4 f5 fw4 white tc ">
    <span className="f4 fw6 green ttc ">{ context.Auth.state.opponent_name}</span> {" has joined the game."}
  </div>
  || null
);

WaitingRoom.propTypes = {
  // bla: PropTypes.string,
};

WaitingRoom.defaultProps = {
  // bla: 'test',
};

export default WaitingRoom;
