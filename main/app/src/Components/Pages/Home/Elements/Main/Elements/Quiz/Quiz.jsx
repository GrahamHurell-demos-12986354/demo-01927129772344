import React from 'react';
import PropTypes from 'prop-types';

import { AppContext } from 'utils'
import { Options } from './Elements'

const Quiz = ({ self, slides, active }) => (
  <div

    id="Quiz"

    style={{

    }}

    className="Quiz flex relative flex-column w-100 h-100 items-center justify-start ">
    {/* 
    <div 
    
    id="Quiz_Response_Background" 
    
    className={ 
      
      ( self.state.quiz_response ? "  quiz_background_enter  o-1 " : "  quiz_background_exit o-0 "  )
      
      +  (  self.state.quiz_response && ( self.state.quiz_success ? " quiz_background_correct " : " quiz_background_incorrect" ) )
    
      + ( " quiz_background " ) 
    
    }
    
    >

    </div> */}

    <ArrayMap self={self} slides={slides} active={active} />

  </div>
);

Quiz.propTypes = {
  // bla: PropTypes.string,
};

Quiz.defaultProps = {
  // bla: 'test',
};

export default Quiz;

Quiz.contextType = AppContext


const ArrayMap = ({ self, slides, active }) => (

  slides && slides.map((arrayitem, index) => (

    <Slide self={self} data={arrayitem} index={index} active={active} />

  )) || <></>

)


const Slide = ({ self, data, index, active }) => (
  index === active &&
  <div

    style={{

      height: '100%',
      width: '100%'

    }}

    className={

      (index === active ? " fadeInUp  z-9" : " fadeOutUp z-01 ")

      + ("  top-0 left-0 absolute flex flex-column w-100 h-100 pv4 ph5-ns ph0 black ")} >


    <Poster poster={data.poster} />

    <MovieTitle title={data.name} />

    <QuizResponse self={self} />

    <Options active={index === active} self={self} slide={data} year={parseInt(data.year)} />

    {/* <Year year={data.year} /> */}

  </div> || null

)

const QuizResponse = ({ self }) => (

  console.log('QuizResponse : self.context.Layout.state ===> ', self.context.Layout.state),

  self.context.Layout.state.quiz_response &&

  <div

    style={{
      bottom: '12vh'
    }}

    className="absolute flex flex-column items-center justify-center pv4 f4 fw5 black w-100 left-0 right-0">

    <span
      className={
        (self.context.Layout.state.quiz_correct ? " pulse " : " shake ")
        + (" ba ph5 pv3 bg--green-highlight  ph3 pv2 br2 bg--blue white fw6 round bw2  ")} >

      {self.context.Layout.state.quiz_response && (self.context.Layout.state.quiz_correct ? "Correct" : "Incorrect")}

    </span>

    <span className="f5 fw5 white tc pv3" >{"You've earned"} <span className=" fw6 f3 ">{self.context.Layout.state.quiz_correct ? " 5 " : " -3 "}</span >{"points"}</span>

  </div>

  || <></>

)

const Year = ({ year }) => (
  <div

    style={{
      height: '10vh'
    }}

    className=" absolute bottom-0 left-0 right-0 w-100 z-9 flex items-center justify-center bt b--black-05  bg--green " >
    <span className=" f2 fw6 black- white ">

      {year}
    </span>
  </div>
)

const MovieTitle = ({ title }) => (
  <div

    style={{
      fontSize: '1.7rem',
      // top: '-5vh'
    }}

    className="relative flex tc fw6 black -white items-center justify-center w-100 "

  >
    {title}
  </div>
)

const Poster = ({ poster }) => (
  <div className=" flex flex-column items-center justify-center w-100 pb5-ns pb4 pt5">
    <div

      style={{

        backgroundImage: 'url("' + poster + '")',
        width: '100px',
        height: '100px'

      }}

      className="grow br3 bg-cover bg-center no-repeat bs-b"

    >

    </div>
  </div>
)