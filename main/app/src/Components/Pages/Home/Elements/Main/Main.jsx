import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@blueprintjs/core'
import { Test } from './Main.styles';

import { AppContext } from 'utils'

import { Welcome, Quiz, Score, Scores, Share, Controls, Loading } from './Elements'
import { shuffle } from './Elements/Quiz/Elements/Options/Methods/index.js'

let isMobile = true

class Main extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {

      active: 0,

      hasError: false,

      quiz_response: false,

      quiz_success: false  

    };
  }

  componentDidMount = async() => {
    
  
    let { context } = this
    let { movies } = context

    console.log('Main context', context)

    let shuffled_movies = await shuffle(movies).then( res => res )

    let a = shuffled_movies.slice(0,8)

    this.setState({

      slides: a,
      ready: true

    })

  }


  componentWillUpdate = (nextProps, nextState) => {

    if( this.context.active !== nextState.active ) {

      this.props.self.setState({

        quiz_response: true,
        quiz_success: false
     
      })

    }

  }

  render () {

    let { state, context } = this
    let { slides, ready, quiz_response, quiz_success } = state
    let { active, player_scores, quiz_active } = context

    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div 
      
      id="Main"

      className="MainWrapper trans-a flex flex-column relative w-100  z-9"
      
      style={

        isMobile 
        
        ?

        {
          height: 'calc( 100% - 20vh)',
          transform: player_scores ? 'translate3d(0,100vh,0)' : 'translate3d(0,00vh,0)'
        } 
        
        : 
        
        {
          height: 'calc( 100% - 20vh)',
          transform: player_scores ? 'translate3d(0,100vh,0)' : 'translate3d(0,00vh,0)'
        } 
      
            }  
      
      >


          { !ready && <Loading /> }

          { ready && 
          
                <>

                      {/* { !quiz_active && <Welcome context={ context } /> } */}

                      { active < 8 && <Quiz self={this} active={ active } slides={ slides } /> }

                      { active >= 8 && <Score context={ context } /> }

                      {/* <Controls self={this} /> */}

                      <Share />
                
                </>
          
          }


      </div>
    );
  }
}

Main.propTypes = {
  // bla: PropTypes.string,
};

Main.defaultProps = {
  // bla: 'test',
};

export default Main;
Main.contextType = AppContext

