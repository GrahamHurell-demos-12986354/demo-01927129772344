import React from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Score.styles';

const Score = ({ self, context }) => (

  console.log('Score : props ==> ', context),

  <div

    id="Score"

    style={{

    }}

    className="Quiz flex relative flex-column justify-between w-100 h-100 items-center "

  >
    {context.Data.state.responses_a.length === 8 && context.Data.state.responses_b.length === 8 &&
      <>  <Score_ opponent={false} tie={context.Data.state.points_a === context.Data.state.points_b} win={context.Data.state.points_a > context.Data.state.points_b} player={context.Auth.state.player_name} points={context.Data.state.points_a} context={context} self={self} />

        <Score_ opponent={true} tie={context.Data.state.points_b === context.Data.state.points_a} win={context.Data.state.points_b > context.Data.state.points_a} player={context.Auth.state.opponent_name} points={context.Data.state.points_b} context={context} self={self} />
      </> || 
      
      <div className=" flex flex-column h-100 items-center justify-center ph5 ">
      
        <div className="  flex flex-column items-center justify-center tc f4 fw5 pb3  ">
      
          {"You scored "}{ context.Data.state.points_a } {" points."}
      
        </div>
        
        <div className="  flex flex-column items-center justify-center tc f5 fw5 pulse ">
      
          {"Hold on, we're waiting for your opponent to complete the quiz before announcing a winner."}
      
        </div>

      </div>

    }
  </div>
);

Score.propTypes = {
  // bla: PropTypes.string,
};

Score.defaultProps = {
  // bla: 'test',
};

export default Score;

const Score_ = ({ self, context, player, points, win, tie, opponent }) => (

  <div

    style={{

      height: tie ? (" 50% ") : (win ? "60%" : "40%")

    }}

    className={

      (tie ? (" bg-light-blue ") : (win ? " bg-green-highlight " : " bg-light-red "))

      + (" flex flex-column h-50 bb b--black-05 w-100 items-center justify-center ")}>


    <div className=" flex flex-column ">

      <span className="flex f5 fw5 items-center justify-center ">{"Points"}</span>

      <span className={tie ? (" f1 ") : (win ? " f1 " : " f2 ") + (" pv3 flex fw6 white  items-center justify-center ")} >{points}</span>

    </div>

    {/* { opponent ? ( "" ) : ( "" ) } */}

    <div className=" flex flex-column ">

      <span className="flex f3 fw5 items-center justify-center ">{opponent ? (win ? player + " won" : player + " lost") : (win ? "You've won" : "You've lost")}</span>

    </div>


  </div>
)