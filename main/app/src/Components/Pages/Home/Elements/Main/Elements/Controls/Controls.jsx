
import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from '@blueprintjs/core'
//import { Test } from './Control.styles';

const Controls = ({ self, context }) => (
  <div

    style={{

      bottom: '10vh',


    }}

    className="ControlWrapper absolute w-100 bottom-0 z-99 ">

    <button

      onClick={() => {

        Prev({ context })

      }}

      style={{
        height: '10vh',
        width: '20vh',
        borderTopRightRadius: '1rem',
        borderBottom: 'none'
      }}

      className="pointer flex items-center justify-center absolute left-0 bottom-0  br bt b--black-05 pointer hover-bg-white-10 bg-transparent "

    >

      <Icon icon={'arrow-left'} iconSize={20} className=" black-50 hover-black " />

    </button>


    <button

      onClick={() => {

        Next({ context })

      }}

      style={{

        height: '10vh',
        width: '20vh',
        borderTopLeftRadius: '1rem',
        borderBottom: 'none'

      }}

      className="pointer flex items-center justify-center absolute right-0 bottom-0  bl bt b--black-05 pointer hover-bg-white-10  bg-transparent "

    >

      <Icon icon={'arrow-right'} iconSize={20} className=" black-50 hover-black " />

    </button>

  </div>
)

export default Controls


const Next = ({ context }) => {

  let responses_a = context.Data.state.responses_a
  let responses_b = context.Data.state.responses_b

  let current_index = context.Control.state.active

  let movies = context.Data.state.movies

  let current_slide = movies[current_index]

  let a = responses_a.filter( b => b.slide._id === current_slide._id )

  // if ( responses_a.length < responses_b.length  ) {
    
  //   alert('Please wait for the other player to proceed.')
    
  //   return
  
  // }
  
  if ( a.length < 1 ) {
  
    alert('Answer the question before proceeding.')
  
    return
  
  }

  let active = context.Control.state.active < context.movies.length ? context.Control.state.active + 1 : context.control.state.active

  context.Control.setState({ active: active })

  context.Layout.setState({

    quiz_state: 'normal',
    quiz_response: false,
    quiz_correct: false

  })

}

const Prev = ({ context }) => {

  let active = context.Control.state.active === 0 ? context.Control.state.active : context.Control.state.active - 1

  context.Control.setState({ active: active })

  context.Layout.setState({

    quiz_state: 'normal',
    quiz_response: false,
    quiz_correct: false

  })

}