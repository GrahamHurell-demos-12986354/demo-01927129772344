import { create_options, shuffle } from './'

const initialize = async(op) => {

    const { self, year } = op

    let options = await create_options(year).then( res => res )

    let shuffled = await shuffle( options ).then( res => res )

    self.setState({

      options: shuffled,
      ready: true

    })

}

export default initialize