import { history } from 'utils'


const submit = async(op) => {
    
    const { self, context, name } = op

    const { Auth, Layout, Socket } = context

    console.log('self.context ', self.context )

    console.log('Login : Submit : context ===>', context)

    let player_id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)

    Layout.setState({ loading: true })

    Auth.setState({

        user: { player_name: name, player_id: player_id },
        player_name: name,
        player_id: player_id,

        isAuthenticated: true
    
    })

    // Auth.submitLogin({ player_name: name, player_id })
    Socket.submitLogin({ player_name: name, player_id })

    history.push('/welcome')

    setTimeout( () => {
        
        Layout.setState({ loading: false })

    }, 1000 )

    // this.socket.emit("answer", JSON.stringify(data));

}

export default submit