import { AppContext } from 'utils'
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import { Login_Form } from './Elements'
import { Wrapper } from '../Elements' 

class Login extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  
  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <Wrapper>

        <AppContext.Consumer>

          { props => (
            
            console.log('Login context ===> ', props ),
            
            <Login_Form self={this} context={props} />

          )}

        </AppContext.Consumer>


      </Wrapper>
    );
  }
}

Login.propTypes = {
  // bla: PropTypes.string,
};

Login.defaultProps = {
  // bla: 'test',
};

export default Login;
