### Move Night Trivia Server stack

## Socket IO

Directory : ./socket-io-relay

Start : 

1 . cd ./socket-io-relay
2 . npm start

## FeathersJs DB API

Directory : ./api

Start : 

1 . cd ./api
2 . npm start

To see all running PM2 processes, run `pm2 list`
To stop all running PM2 processes, run `pm2 stop all`
To delete all running PM2 processes, run `pm2 delete all`